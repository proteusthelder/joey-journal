---
title: Session 5
date: 2019-09-30
---

As a special treat, we had Mommy and Artie here today!

Ms. Chris was very excited to see all of the new words that Joey's been saying!

* [ ] Daddy needs to gin up something to plot the number of words over time for
  Ms. Chris.  Thinking a Python plot of the word count by the commit date?

Ms. Chris and Joey played with a marble maze, and practiced "ball" while working
on _drop_

Joey was able to say "bump-bump-bump-bump-bump!" as the marble went down the
track.

Artie helped by putting the marble in the top and describing every step of the
path: "In, spin, bump-bump-bump-bump-bump, out!"

Trying to put two words together: "Ball in"
Said them separately after prompting

Try to demand 2 words together when he wants things: "stool up" etc.

