---
title: Session 3
date: 2019-09-09
---

Ms. Chris pulled up as Joey and Daddy were going into the house.  Joey was so
excited he rushed over to move his table closer to the couch for the session!

Ms. Chris asked about any new words that were shocking to us, the only one I
could really remember was "Monkey" but I asked Mommy if there were any more.

Ms. Chris got Joey to say "Go" when SpyHop was a little too in the middle.  Also
got him to say "Me" when asked "Who wants to put the duck away?"

Try to make sure that Joey is encouraged to use other words, vice using
"Wheeee!" for everything.  Also try to encourage him to say "me" - "Who wants a
cookie?" :)

When Joey is done with all of his food, try to get him to say /All Gone!/

In general, we should be trying to reinforce the various new words he learns
each week.  This week, we should focus on:

1. Pull
1. Me
1. All Done
1. Heart



