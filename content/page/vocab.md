---
title: Vocabulary
subtitle: Growing Every Week!
comments: false
---

1. up
1. down
1. yes
1. no
1. blue
1. purple
1. mommy
1. daddy
1. mom
1. dad
1. in
1. outside (ousside)
1. ball
1. cup (up-a)
1. whale (when prompted by Artie)
1. RaRa (his name for Artie)
1. Gigah
1. one, two, three, five, six, eight
1. plate
1. bowl
1. cheese
1. nothing (just heard this one once)
1. monkey (referring to the monkey garage)
1. me
1. heart
1. pull
1. please (peas)
1. yay
1. cat
1. out
1. moon (on its way)
1. fly (like the bug)
1. hat
1. throw
1. poop
1. phone
1. kick
1. eat
1. shoes
1. Elmo
1. Mickey
1. stool
1. bubble
